import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  
  { path: 'test', loadChildren: () => import('src/app/modules/product/product.module').then(m => m.ProductModule) },
    { path: '', pathMatch: 'full', redirectTo: '/test/datatable' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
