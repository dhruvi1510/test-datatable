import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

export interface PeriodicElement {
  name: string;
  id: number;
  createdat: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {id: 1, name: 'Hydrogen',createdat:'a' },
  {id: 2, name: 'Helium',createdat:'b'},
  {id: 3, name: 'Lithium',createdat:'c' },
  {id: 4, name: 'Beryllium',createdat:'d'},
  {id: 5, name: 'Boron',createdat:'e' },
  {id: 6, name: 'Carbon', createdat:'f'},
  {id: 7, name: 'Nitrogen', createdat:'g'},
  {id: 8, name: 'Oxygen', createdat:'h'},
  {id: 9, name: 'Fluorine',createdat:'i' },
  {id: 10, name: 'Neon',createdat:'j'},
  {id: 11, name: 'Sodium', createdat:'k'},
  {id: 12, name: 'Magnesium', createdat:'l'},
  {id: 13, name: 'Aluminum',createdat:'m'},
  {id: 14, name: 'Silicon', createdat:'n'},
  {id: 15, name: 'Phosphorus', createdat:'o'}
];

@Component({
  selector: 'app-test-data-table',
  templateUrl: './test-data-table.component.html',
  styleUrls: ['./test-data-table.component.scss']
})
export class TestDataTableComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'createdat'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  
  allTestDataTable:any;
  

  constructor(private route:Router,private HttpClient:HttpClient) { }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
  }

}
